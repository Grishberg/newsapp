package com.grishberg.newsapp;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import com.grishberg.newsapp.injection.DaggerAppComponent;
import com.grishberg.newsapp.injection.DbModule;
import com.grishberg.newsapp.injection.TestRestModule;
import com.grishberg.newsapp.mvp.data.db.DatabaseHelper;
import com.grishberg.newsapp.mvp.data.models.RestResponse;
import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.grishberg.newsapp.mvp.data.rest.TestStubInterceptor;
import com.grishberg.newsapp.mvp.presenters.NewsDetailPresenter;
import com.grishberg.newsapp.mvp.presenters.NewsListPresenter;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import rx.observers.TestSubscriber;

/**
 * Created by grishberg on 02.07.16.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class SuccessRestTest {
    private static final String TAG = SuccessRestTest.class.getSimpleName();
    public static final int ARTICLE_ID = 1;

    /**
     * Инициализация тестового Retrofit сервиса
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        App app = (App) InstrumentationRegistry.getTargetContext().getApplicationContext();
        app.initComponents(DaggerAppComponent
                .builder()
                .dbModule(new DbModule(new DatabaseHelper(app)))
                .restModule(new TestRestModule(ApiConst.Api.END_POINT, true))
                .build()
        );
    }

    /**
     * Тестирование регистрации
     *
     * @throws Exception
     */
    @Test
    public void testGetNewsList() throws Exception {
        NewsListPresenter newsListPresenter = new NewsListPresenter();
        TestSubscriber<RestResponse<List<NewsItem>>> testSubscriber = new TestSubscriber<>();

        newsListPresenter
                .getNewsList()
                .subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        List<RestResponse<List<NewsItem>>> newsList = testSubscriber.getOnNextEvents();

        Assert.assertNotNull(newsList);
        RestResponse<List<NewsItem>> response = newsList.get(0);
        Assert.assertNotNull(response);
        Assert.assertEquals(1, newsList.size());
        Assert.assertEquals(TestStubInterceptor.TEST_TITLE, response.getData().get(0).getTitle());
        Assert.assertEquals(TestStubInterceptor.DATE1, response.getData().get(0).getDateCreate());
        Assert.assertEquals(TestStubInterceptor.DATE2, response.getData().get(0).getDateUpdate());
    }

    @Test
    public void testGetNews() throws Exception {
        NewsDetailPresenter newsDetailPresenter = new NewsDetailPresenter();
        TestSubscriber<RestResponse<NewsDetails>> testSubscriber = new TestSubscriber<>();

        newsDetailPresenter
                .getNewsDetail(ARTICLE_ID)
                .subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        List<RestResponse<NewsDetails>> newsList = testSubscriber.getOnNextEvents();

        Assert.assertNotNull(newsList);
        NewsDetails response = newsList.get(0).getData();
        Assert.assertNotNull(response);
        Assert.assertEquals(1, newsList.size());
        Assert.assertEquals(TestStubInterceptor.TEST_TITLE, response.getTitle());
        Assert.assertEquals(TestStubInterceptor.TEST_TEXT, response.getText());
        Assert.assertEquals(TestStubInterceptor.DATE1, response.getDateCreate());
        Assert.assertEquals(TestStubInterceptor.DATE2, response.getDateUpdate());
    }
}
