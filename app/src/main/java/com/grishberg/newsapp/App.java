package com.grishberg.newsapp;

import android.content.Context;
import android.os.StrictMode;

import com.arellomobile.mvp.MvpApplication;
import com.crashlytics.android.Crashlytics;
import com.grishberg.newsapp.injection.AppComponent;
import com.grishberg.newsapp.injection.DaggerAppComponent;
import com.grishberg.newsapp.injection.DbModule;
import com.grishberg.newsapp.injection.RestModule;
import com.grishberg.newsapp.mvp.data.db.DatabaseHelper;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import io.fabric.sdk.android.Fabric;

/**
 * Created by grishberg on 30.06.16.
 */
public class App extends MvpApplication {
    private static final String TAG = App.class.getSimpleName();
    private static AppComponent appComponent;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
/*
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }
        */

        initUil();
    }

    /**
     * Инициализация краш аналитики
     */
    private void initFabric() {
        Fabric.with(this, new Crashlytics());
    }

    /**
     * Инициализация Universal Image Loader
     */
    private void initUil() {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration
                .Builder(getApplicationContext());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        if (BuildConfig.DEBUG) {
            config.writeDebugLogs(); // Remove for release app
        }
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public synchronized static AppComponent getAppComponent() {
        if (appComponent == null) {
            initComponents(DaggerAppComponent
                    .builder()
                    .dbModule(new DbModule(new DatabaseHelper(context)))
                    .restModule(new RestModule(ApiConst.Api.END_POINT))
                    .build()
            );
        }
        return appComponent;
    }

    public static void initComponents(AppComponent component) {
        appComponent = component;
    }
}
