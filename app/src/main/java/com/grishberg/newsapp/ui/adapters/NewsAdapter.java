package com.grishberg.newsapp.ui.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.grishberg.datafacade.controllers.BaseRecyclerAdapter;
import com.grishberg.datafacade.data.ListResult;
import com.grishberg.newsapp.ApiConst;
import com.grishberg.newsapp.App;
import com.grishberg.newsapp.R;
import com.grishberg.newsapp.injection.AppComponent;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by grishberg on 01.07.16.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private static final String TAG = NewsAdapter.class.getSimpleName();
    private View.OnClickListener onItemClickListener;
    private List<NewsItem> items = null;
    private boolean isAnimationEnabled;
    private final Context context;

    @Inject
    ImageLoader imageLoader;
    @Inject
    DisplayImageOptions displayImageOptions;
    private int lastPosition;
    private final Typeface comfortaaFace;

    public NewsAdapter(@NonNull Context context) {
        this.context = context;
        App.getAppComponent().inject(this);
        comfortaaFace = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Bold.ttf");
    }

    public void setItems(List<NewsItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setAnimationEnabled(boolean animationEnabled) {
        isAnimationEnabled = animationEnabled;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        NewsItem item = items.get(position);
        holder.bind(item);
        if (isAnimationEnabled) {
            setAnimation(holder.itemView, position);
        }
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    /**
     * Set click listener on items
     *
     * @param onItemClickListener
     */
    public void setOnItemClickListener(View.OnClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * сброс анимации у скрытых элементов
     * иначе во время быстрого скролла анимация работает некорректно,
     * так как переиспользуются те же элементы
     *
     * @param holder
     */
    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.clearAnimation();
    }

    /**
     * Анимация появления новых элементов
     *
     * @param viewToAnimate
     * @param position
     */
    private void setAnimation(View viewToAnimate, int position) {
        // Отображать анимацию только для новых элементов
        if (position > lastPosition) {
            Animation moveAnimation = AnimationUtils.loadAnimation(context,
                    R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(moveAnimation);
            lastPosition = position;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvTitle;
        public final ImageView ivPreview;
        public boolean isImageLoaded;

        public ViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvTitle.setTypeface(comfortaaFace);
            ivPreview = (ImageView) view.findViewById(R.id.ivPreview);
            itemView.setOnClickListener(onItemClickListener);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvTitle.getText() + "'";
        }

        public void bind(NewsItem item) {
            tvTitle.setText(item.getTitle());
            loadImage(item);
            itemView.setTag(item);
        }

        /**
         * Загрузка изображения по ссылке
         *
         * @param item
         */
        private void loadImage(NewsItem item) {
           
            if (TextUtils.isEmpty(item.getPreview())) {
                ivPreview.setImageBitmap(null);
                return;
            }
            imageLoader.displayImage(ApiConst.Api.END_POINT + item.getPreview(),
                    ivPreview, displayImageOptions,
                    new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            Log.d(TAG, "onLoadingStarted: ");
                            ivPreview.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            Log.d(TAG, "onLoadingFailed: ");
                            ivPreview.setVisibility(View.INVISIBLE);
                            isImageLoaded = true;
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            Log.d(TAG, "onLoadingComplete: ");
                            ivPreview.setVisibility(View.VISIBLE);
                            //if(isCashed) {
                            //    Animation fadeInAnimation = AnimationUtils
                            //            .loadAnimation(context, R.anim.fadein);
                            //    ivPreview.startAnimation(fadeInAnimation);
                            //}
                        }
                    });
        }

        // отключение анимации
        public void clearAnimation() {
            itemView.clearAnimation();
        }
    }
}
