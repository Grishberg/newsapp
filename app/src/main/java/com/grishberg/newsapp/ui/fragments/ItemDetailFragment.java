package com.grishberg.newsapp.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.grishberg.newsapp.R;
import com.grishberg.newsapp.mvp.common.MvpFragment;
import com.grishberg.newsapp.mvp.presenters.NewsDetailPresenter;
import com.grishberg.newsapp.mvp.views.NewsDetailView;
import com.grishberg.newsapp.ui.activities.ItemDetailActivity;
import com.grishberg.newsapp.ui.activities.ItemListActivity;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends MvpFragment implements NewsDetailView {
    private static final String TAG = ItemDetailFragment.class.getSimpleName();
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_ARTICLE_TITLE = "title";
    public static final String FONT_NAME = "fonts/NotoSerif-Regular.ttf";

    @InjectPresenter
    NewsDetailPresenter newsDetailPresenter;
    /**
     * The dummy content this fragment is presenting.
     */
    private CollapsingToolbarLayout appBarLayout;
    private WebView wvContent;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            int id = getArguments().getInt(ARG_ITEM_ID);
            String title = getArguments().getString(ARG_ARTICLE_TITLE);
            Activity activity = this.getActivity();
            if (newsDetailPresenter == null) {
                Log.e(TAG, "onCreate: newsDetailPresenter is null");
                return;
            }
            newsDetailPresenter.getNewsDetail(id);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);
        wvContent = (WebView) rootView.findViewById(R.id.wvContent);
        WebSettings settings = wvContent.getSettings();
        settings.setDefaultTextEncodingName("utf-8");

        Typeface face = Typeface.createFromAsset(
                getActivity().getAssets(), FONT_NAME);
        //wvContent.setTypeface(face);
        return rootView;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void hideError() {

    }

    /**
     * Отображение статьи
     *
     * @param htmlBody
     */
    @Override
    public void showNews(String htmlBody) {
        Log.d(TAG, "showNews: simple text");
        wvContent.loadData(getHtmlData(getContext(), htmlBody), "text/html; charset=utf-8", "utf-8");
    }

    private String getHtmlData(Context context, String data) {
        String head = "<head><style>@font-face {font-family: 'NotoSerif';src: url('file://"
                + context.getFilesDir().getAbsolutePath()
                + "/NotoSerif-Regular.ttf');}body {font-family: 'NotoSerif';}</style></head>";
        String htmlData = "<html>" + head + "<body>" + data + "</body></html>";
        return htmlData;
    }
}
