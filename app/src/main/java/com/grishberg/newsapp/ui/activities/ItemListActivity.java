package com.grishberg.newsapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.grishberg.datafacade.data.ListResult;
import com.grishberg.newsapp.mvp.common.MvpAppCompatActivity;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.grishberg.newsapp.mvp.presenters.NewsListPresenter;
import com.grishberg.newsapp.mvp.views.NewsListView;
import com.grishberg.newsapp.ui.adapters.DividerItemDecoration;
import com.grishberg.newsapp.ui.adapters.NewsAdapter;
import com.grishberg.newsapp.ui.fragments.ItemDetailFragment;
import com.grishberg.newsapp.R;
import com.grishberg.newsapp.dummy.DummyContent;

import java.util.List;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends MvpAppCompatActivity implements NewsListView, View.OnClickListener {

    private static final String TAG = ItemListActivity.class.getSimpleName();
    private static final String BUNDLE_RECYCLER_LAYOUT = "classname.recycler.layout";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    @InjectPresenter
    NewsListPresenter newsListPresenter;

    private boolean mTwoPane;
    private NewsAdapter adapter;
    private RecyclerView recyclerView;
    private int lastFirstVisiblePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: " + savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        initRecyclerView(savedInstanceState);

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
        newsListPresenter.getNewsList();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState: ");
        super.onRestoreInstanceState(savedInstanceState);
        Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
        recyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);

    }

    private void initRecyclerView(Bundle savedInstanceState) {
        recyclerView = (RecyclerView) findViewById(R.id.item_list);
        assert recyclerView != null;
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL_LIST));
        adapter = new NewsAdapter(this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(getApplicationContext(), R.string.errorConnection, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideError() {

    }

    @Override
    public void showNewsList(List<NewsItem> newsItems) {
        Log.d(TAG, "showNewsList: ");
        adapter.setItems(newsItems);
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        NewsItem newsItem = (NewsItem) view.getTag();
        ItemDetailActivity.navigate(this, view.findViewById(R.id.ivPreview), newsItem);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: ");
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT,
                recyclerView.getLayoutManager().onSaveInstanceState());
    }

}
