package com.grishberg.newsapp.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.grishberg.newsapp.ApiConst;
import com.grishberg.newsapp.App;
import com.grishberg.newsapp.mvp.common.MvpAppCompatActivity;
import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.grishberg.newsapp.mvp.views.NewsDetailView;
import com.grishberg.newsapp.ui.fragments.ItemDetailFragment;
import com.grishberg.newsapp.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import javax.inject.Inject;

/**
 * An activity representing a single Item detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ItemListActivity}.
 */
public class ItemDetailActivity extends AppCompatActivity {
    private static final String TAG = ItemDetailActivity.class.getSimpleName();
    private static final String EXTRA_IMAGE = "com.grishberg.newsapp.extraImage";
    private static final String EXTRA_TITLE = "com.grishberg.newsapp.extraTitle";
    private static final String EXTRA_ID = "com.grishberg.newsapp.extraId";
    private CollapsingToolbarLayout collapsingToolbarLayout;
    @Inject
    ImageLoader imageLoader;
    @Inject
    DisplayImageOptions displayImageOptions;
    private Typeface comfortaaFace;

    public static void navigate(AppCompatActivity activity, View transitionImage,
                                NewsItem newsItem) {
        Intent intent = new Intent(activity, ItemDetailActivity.class);
        intent.putExtra(EXTRA_IMAGE, newsItem.getPreview());
        intent.putExtra(EXTRA_TITLE, newsItem.getTitle());
        intent.putExtra(EXTRA_ID, newsItem.getId());

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_IMAGE);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);

        setContentView(R.layout.activity_item_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        assert tvTitle == null;
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        ViewCompat.setTransitionName(collapsingToolbarLayout, EXTRA_IMAGE);

        comfortaaFace = Typeface.createFromAsset(getAssets(), "fonts/Comfortaa-Bold.ttf");
        String itemTitle = getIntent().getStringExtra(EXTRA_TITLE);
        tvTitle.setText(itemTitle);
        tvTitle.setTypeface(comfortaaFace);

        // цвет заголовка в раскрытом состоянии
        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(android.R.color.transparent));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        //collapsingToolbarLayout.setExpandedTitleTextAppearance();

        final ImageView image = (ImageView) findViewById(R.id.ivNewsPreview);
        if (!TextUtils.isEmpty(getIntent().getStringExtra(EXTRA_IMAGE))) {
            imageLoader.displayImage(ApiConst.Api.END_POINT + getIntent().getStringExtra(EXTRA_IMAGE),
                    image);
        }

        // like fab
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setVisibility(View.GONE);
            fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show());
        }

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            int id = getIntent().getIntExtra(EXTRA_ID, 1);
            String imageUrl = getIntent().getStringExtra(EXTRA_IMAGE);
            String title = getIntent().getStringExtra(EXTRA_TITLE);

            arguments.putInt(ItemDetailFragment.ARG_ITEM_ID, id);
            arguments.putString(ItemDetailFragment.ARG_ARTICLE_TITLE, title);
            ItemDetailFragment fragment = new ItemDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment, ItemDetailFragment.class.getSimpleName())
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
