package com.grishberg.newsapp.injection;

import com.grishberg.newsapp.mvp.data.rest.TestStubInterceptor;

import dagger.Module;
import okhttp3.Interceptor;

/**
 * Created by grishberg on 02.07.16.
 * Модуль для заглушки API
 */
@Module
public class TestRestModule extends RestModule {
    private static final String TAG = TestRestModule.class.getSimpleName();
    private boolean isSuccessResponses;
    public TestRestModule(String baseUrl, boolean isSuccessResponses) {
        super(baseUrl);
        this.isSuccessResponses = isSuccessResponses;
    }

    /**
     * interceptor для логирования
     *
     * @return
     */
    @Override
    Interceptor provideInterceptor() {
        TestStubInterceptor interceptor = new TestStubInterceptor(isSuccessResponses);
        return interceptor;
    }
}
