package com.grishberg.newsapp.injection;

import android.content.res.Resources;

import com.grishberg.newsapp.mvp.data.rest.TestStubInterceptor;
import com.grishberg.newsapp.mvp.presenters.NewsDetailPresenter;
import com.grishberg.newsapp.mvp.presenters.NewsListPresenter;
import com.grishberg.newsapp.ui.activities.ItemDetailActivity;
import com.grishberg.newsapp.ui.adapters.NewsAdapter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by grishberg on 29.06.16.
 */
@Singleton
@Component(modules = {RestModule.class, AppModule.class, UilModule.class,
        DbModule.class})
public interface AppComponent {
    void inject(NewsListPresenter newsListPresenter);

    void inject(NewsDetailPresenter newsDetailPresenter);

    void inject(TestStubInterceptor testStubInterceptor);

    void inject(NewsAdapter newsAdapter);

    void inject(ItemDetailActivity itemDetailActivity);
}