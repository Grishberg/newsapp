package com.grishberg.newsapp.injection;

import android.content.Context;
import android.util.Log;

import com.grishberg.newsapp.mvp.data.db.DatabaseHelper;
import com.grishberg.newsapp.mvp.data.db.NewsDetailDao;
import com.grishberg.newsapp.mvp.data.db.NewsItemsDao;
import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created on 08.07.16.
 *
 * @author g
 */
@Module
public class DbModule {
    private static final String TAG = DbModule.class.getSimpleName();
    private final DatabaseHelper databaseHelper;

    public DbModule(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @Provides
    @Singleton
    public NewsItemsDao provideNewsItemsDao() {
        try {
            return new NewsItemsDao(databaseHelper.getDao(NewsItem.class));
        } catch (SQLException e) {
            Log.e(TAG, "provideNewsItemsDao: ", e);
        }
        return null;
    }

    @Provides
    @Singleton
    public NewsDetailDao provideNewsDetailDao() {
        try {
            return new NewsDetailDao(databaseHelper.getDao( NewsDetails.class));
        } catch (SQLException e) {
            Log.e(TAG, "provideNewsDetailDao: ", e);
        }
        return null;
    }
}
