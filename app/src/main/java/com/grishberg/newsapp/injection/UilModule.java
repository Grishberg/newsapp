package com.grishberg.newsapp.injection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grishberg on 02.07.16.
 */
@Module
public class UilModule {
    private static final String TAG = UilModule.class.getSimpleName();

    @Provides
    @Singleton
    ImageLoader provideImageLoader() {
        return ImageLoader.getInstance();
    }

    @Provides
    @Singleton
    DisplayImageOptions provideOptions(){
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
}
