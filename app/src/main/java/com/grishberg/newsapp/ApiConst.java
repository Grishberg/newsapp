package com.grishberg.newsapp;

/**
 * Created by grishberg on 30.06.16.
 */
public final class ApiConst {
    private static final String TAG = ApiConst.class.getSimpleName();

    public final class Api {
        private Api() {
        }

        public static final String END_POINT = "http://wtd.april32.com/";

    }

    public final class Methods{
        private Methods(){}

        public static final String GET_ARTICLE_LIST = "/api/v1/articles";
        public static final String GET_ARTICLE = "/api/v1/articles/{id}";
    }
}
