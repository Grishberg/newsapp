package com.grishberg.newsapp.mvp.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.MvpDelegate;

/**
 * Created by grishberg on 01.07.16.
 */
public class MvpFragment extends Fragment {
    private Bundle mTemporaryBundle;// required for view destroy/restore
    private MvpDelegate<? extends MvpFragment> mMvpDelegate;

    public MvpFragment()
    {
        mTemporaryBundle = null;
    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getMvpDelegate().onCreate(savedInstanceState);
    }

    public void onStart()
    {
        super.onStart();

        getMvpDelegate().onAttach();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        getMvpDelegate().onDetach();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        getMvpDelegate().onDestroy();
    }

    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        getMvpDelegate().onSaveInstanceState(outState);
    }

    public MvpDelegate getMvpDelegate()
    {
        if (mMvpDelegate == null)
        {
            mMvpDelegate = new MvpDelegate<>(this);
        }

        return mMvpDelegate;
    }
}