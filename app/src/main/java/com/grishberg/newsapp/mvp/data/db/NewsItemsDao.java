package com.grishberg.newsapp.mvp.data.db;

import android.database.Cursor;
import android.util.Log;

import com.grishberg.datafacade.data.ListResult;
import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.j256.ormlite.android.AndroidDatabaseResults;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by grishberg on 01.07.16.
 */
public class NewsItemsDao {
    private static final String TAG = NewsItemsDao.class.getSimpleName();
    private final Dao<NewsItem, Void> dao;

    public NewsItemsDao(Dao<NewsItem, Void> dao) throws SQLException {
        this.dao = dao;
    }

    /**
     * Вернуть список статей из кэша
     * @return
     */
    public List<NewsItem> getNewsList() {
        List<NewsItem> newsDetailList = null;
        try {
            QueryBuilder<NewsItem, Void> queryBuilder = dao.queryBuilder()
                    .orderBy(NewsItem.DATE_UPDATE, false);
            PreparedQuery<NewsItem> preparedQuery = queryBuilder.prepare();
            newsDetailList = dao.query(preparedQuery);
            if (newsDetailList.size() == 0) {
                return null;
            }
        } catch (SQLException e) {
            Log.e(TAG, "getNewsList: ", e);
        }
        return newsDetailList;
    }

    /**
     * Добавить данные в бд
     *
     * @param item элемент списка статей
     * @return возвращается true, если результат успешный
     */
    public boolean add(NewsItem item) {
        try {
            dao.createOrUpdate(item);
            return true;
        } catch (SQLException e) {
            Log.e(TAG, "add: ", e);
            return false;
        }
    }

    /**
     * Добавление списка в бд
     *
     * @param newsItems
     */
    public void add(List<NewsItem> newsItems) {
        for (int i = 0; i < newsItems.size(); i++) {
            add(newsItems.get(i));
        }
    }

    /**
     * Удалить старые данные из кэша
     */
    public void removeOldData() {
        //TODO: продумать алгоритм удаление старых данных
    }
}
