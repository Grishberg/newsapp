package com.grishberg.newsapp.mvp.data.db;

import com.grishberg.datafacade.data.BaseResult;
import com.grishberg.datafacade.data.DataReceiveObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grishberg on 01.07.16.
 */
public class BaseListResultArrayImpl<T> implements BaseResult {
    private static final String TAG = BaseListResultArrayImpl.class.getSimpleName();
    public BaseListResultArrayImpl() {
    }
    protected List<T> data;

    public BaseListResultArrayImpl(List<T> data) {
        this.data = data;
    }

    @Override
    public void addDataReceiveObserver(DataReceiveObserver observer) {
    }

    @Override
    public void removeDataReceiveObserver(DataReceiveObserver observer) {
    }

    @Override
    public boolean isLoaded() {
        return false;
    }

    @Override
    public void release() {
    }
}
