package com.grishberg.newsapp.mvp.common;

import android.util.Log;

import com.grishberg.newsapp.mvp.common.errors.GetNewsListException;
import com.grishberg.newsapp.mvp.data.models.RestResponse;
import com.j256.ormlite.dao.Dao;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Date: 18.01.2016
 * Time: 14:37
 *
 * @author Yuri Shmakov
 *         edited Grigoriy Rylov
 */
public class RxUtils {
    private static final String TAG = RxUtils.class.getSimpleName();

    public static <T> Observable<RestResponse<T>> wrapRetrofitCall(final Call<T> call,
                                                                   final CacheListener<T> cacheListener) {
        Log.d(TAG, "wrapRetrofitCall: ");
        return Observable.create(subscriber -> {
            final Response<T> execute;
            Log.d(TAG, "wrapRetrofitCall: execute " + Thread.currentThread());
            T cache = cacheListener != null ? cacheListener.getCache(0) : null;
            if (cache != null) {
                Log.d(TAG, "wrapRetrofitCall: data from cache");
                subscriber.onNext(new RestResponse<>(cache, true));
            }

            try {
                Log.d(TAG, "wrapRetrofitCall: call.execute");
                execute = call.execute();
            } catch (IOException e) {
                subscriber.onError(e);
                return;
            }

            if (execute.isSuccess()) {
                subscriber.onNext(new RestResponse<>(execute.body(), false));
                subscriber.onCompleted();
            } else {
                subscriber.onError(new GetNewsListException(execute.errorBody()));
            }
        });
    }

    public static <T> Observable<T> wrapAsync(Observable<T> observable) {
        return wrapAsync(observable, Schedulers.io());
    }

    public static <T> Observable<T> wrapAsync(Observable<T> observable, Scheduler scheduler) {
        return observable
                .materialize()
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread()).<T>dematerialize();
    }

    public interface CacheListener<T> {
        T getCache(int id);
    }
}
