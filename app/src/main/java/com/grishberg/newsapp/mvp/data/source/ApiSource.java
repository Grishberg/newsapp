package com.grishberg.newsapp.mvp.data.source;

import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;

import java.util.List;

import rx.Observable;

/**
 * Created by grishberg on 16.07.16.
 */
public interface ApiSource {
    Observable<List<NewsItem>> getNewsList();
    Observable<NewsDetails> getNewsDetail(int articleId);
}
