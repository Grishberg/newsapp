package com.grishberg.newsapp.mvp.data.db;

import com.grishberg.datafacade.data.ListResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grishberg on 01.07.16.
 */
public class ListResultArrayImpl<T> extends BaseListResultArrayImpl<T>
        implements ListResult<T> {
    private static final String TAG = ListResultArrayImpl.class.getSimpleName();

    public ListResultArrayImpl(List<T> data) {
       super(data);
    }

    @Override
    public int getCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public T getItem(int index) {
        return data != null ? data.get(index) : null;
    }
}
