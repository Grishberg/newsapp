package com.grishberg.newsapp.mvp.data.db;

import android.util.Log;

import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created on 08.07.16.
 *
 * @author g
 */
public class NewsDetailDao {
    private static final String TAG = NewsDetailDao.class.getSimpleName();

    private final Dao<NewsDetails, Void> dao;

    public NewsDetailDao(Dao<NewsDetails, Void> dao) {
        this.dao = dao;
    }

    /**
     * Добавить данные в бд
     *
     * @param item
     * @return
     */
    public void add(NewsDetails item) {
        try {
            dao.createOrUpdate(item);
        } catch (SQLException e) {
            Log.e(TAG, "add: ", e);
        }
    }

    public NewsDetails get(int id) {
        List<NewsDetails> newsDetailList = null;
        try {
            QueryBuilder<NewsDetails, Void> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq(NewsDetails.ID, id);
            PreparedQuery<NewsDetails> preparedQuery = queryBuilder.prepare();
            newsDetailList = dao.query(preparedQuery);
        } catch (SQLException e) {
            Log.e(TAG, "get: ", e);
        }
        return newsDetailList != null && newsDetailList.size() > 0 ? newsDetailList.get(0) : null;
    }
}
