package com.grishberg.newsapp.mvp.views;

import android.text.Spanned;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.grishberg.datafacade.data.ListResult;
import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;

/**
 * Created by grishberg on 01.07.16.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface NewsDetailView extends MvpView {
    void showProgress();

    void hideProgress();

    void showError(String message);

    void hideError();

    void showNews(String htmlBody);
}
