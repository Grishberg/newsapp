package com.grishberg.newsapp.mvp.data.source;

import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;

import java.util.List;

/**
 * Created by grishberg on 16.07.16.
 */
public interface DbSource extends ApiSource {
    void saveNewsList(List<NewsItem> items);
    void saveNewsDetail(NewsDetails newsDetails);
}
