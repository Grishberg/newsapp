package com.grishberg.newsapp.mvp.presenters;

import android.content.res.Resources;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.grishberg.newsapp.App;
import com.grishberg.newsapp.mvp.common.RxUtils;
import com.grishberg.newsapp.mvp.data.api.ApiService;
import com.grishberg.newsapp.mvp.data.db.ListResultArrayImpl;
import com.grishberg.newsapp.mvp.data.db.NewsDetailDao;
import com.grishberg.newsapp.mvp.data.models.RestResponse;
import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.grishberg.newsapp.mvp.views.NewsDetailView;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by grishberg on 01.07.16.
 * Презентер для логики загрузки детальной информации о статье
 */
@InjectViewState
public class NewsDetailPresenter extends MvpPresenter<NewsDetailView> {
    private static final String TAG = NewsDetailPresenter.class.getSimpleName();
    @Inject
    ApiService apiService;
    @Inject
    NewsDetailDao newsDetailDao;

    public NewsDetailPresenter() {
        super();
        Log.d(TAG, "NewsListPresenter: ");
        App.getAppComponent().inject(this);
    }

    /**
     * Извлечение детайльной информации о новости, дальнейшая предобработка
     *
     * @param id идентификатор новости
     * @return возвращается Observable<NewsDetails> для тестирования
     */
    public Observable<RestResponse<NewsDetails>> getNewsDetail(int id) {
        Log.d(TAG, "getNewsDetail: ");

        if (getViewState() == null) {
            Log.e(TAG, "getNewsDetail: getViewState() is null");
            return null;
        }
        getViewState().showProgress();

        // вызов Retrofit
        Observable<RestResponse<NewsDetails>> newsObservable = RxUtils
                .wrapRetrofitCall(apiService.getNewsDetails(id), articleId -> {
                    // извлечь данные из кэша
                    Log.d(TAG, "getNewsDetail: check cache");
                    return newsDetailDao.get(id);
                });

        RxUtils.wrapAsync(newsObservable, Schedulers.computation())
                .flatMap(newsDetailsRestResponse -> {
                    Log.d(TAG, "getNewsDetail: flatMap:");
                    if (newsDetailsRestResponse == null || newsDetailsRestResponse.getData() == null) {
                        Log.e(TAG, "getNewsDetail: data is empty");
                        return null;
                    }
                    if (!newsDetailsRestResponse.isCached()) {
                        Log.d(TAG, "getNewsDetail: flatMap: store cache");
                        newsDetailDao.add(newsDetailsRestResponse.getData());
                    }
                    return Observable.just(newsDetailsRestResponse.getData().getText());
                })
                .subscribe(newsContent -> {
                    Log.d(TAG, "getNewsDetail: show data");
                    getViewState().hideProgress();
                    getViewState().showNews(newsContent.replace("img src=","img width=\"100%\" src="));
                }, exception -> {
                    Log.e(TAG, "getNewsDetail: exc", exception);
                    getViewState().hideProgress();
                    getViewState().showError(exception.getMessage());
                });
        return newsObservable;
    }
}
