package com.grishberg.newsapp.mvp.data.db;

import com.grishberg.datafacade.data.SingleResult;

/**
 * Created on 09.07.16.
 *
 * @author g
 */
public class SingleResultImpl<T> extends BaseListResultArrayImpl<T> implements SingleResult<T> {
    private static final String TAG = SingleResultImpl.class.getSimpleName();

    @Override
    public T getItem() {
        return data != null ? data.get(0) : null;
    }

    @Override
    public T getItemSync() {
        return null;
    }
}
