package com.grishberg.newsapp.mvp.data.models;

/**
 * Created on 12.07.16.
 *
 * @author g
 */
public class RestResponse<T> {
    private static final String TAG = RestResponse.class.getSimpleName();
    private boolean isCached;
    private T data;
    private int errorCode;

    public RestResponse(T data, boolean isCached) {
        this.data = data;
        this.isCached = isCached;
    }

    public boolean isCached() {
        return isCached;
    }

    public void setCached(boolean cached) {
        isCached = cached;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
