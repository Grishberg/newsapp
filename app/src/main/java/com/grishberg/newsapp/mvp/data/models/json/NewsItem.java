package com.grishberg.newsapp.mvp.data.models.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Модель для хранения списка статей
 */
@DatabaseTable(tableName = "news_items")
public class NewsItem {
    public static final String TITLE = "title";
    public static final String DATE_CREATE = "date_create";
    public static final String DATE_UPDATE = "date_update";
    public static final String PREVIEW = "preview";
    public static final String ID = "id";

    @SerializedName(ID)
    @Expose
    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER, columnName = ID, id = true)
    private int id;

    @SerializedName(TITLE)
    @Expose
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = TITLE)
    private String title;

    @SerializedName(DATE_CREATE)
    @Expose
    @DatabaseField(canBeNull = false, dataType = DataType.DATE, columnName = DATE_CREATE)
    private Date dateCreate;

    @SerializedName(DATE_UPDATE)
    @Expose
    @DatabaseField(canBeNull = false, dataType = DataType.DATE, columnName = DATE_UPDATE)
    private Date dateUpdate;

    @SerializedName(PREVIEW)
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = PREVIEW)
    private String preview;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The dateCreate
     */
    public Date getDateCreate() {
        return dateCreate;
    }

    /**
     * @param dateCreate The date_create
     */
    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    /**
     * @return The dateUpdate
     */
    public Date getDateUpdate() {
        return dateUpdate;
    }

    /**
     * @param dateUpdate The date_update
     */
    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }
}
