package com.grishberg.newsapp.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.grishberg.newsapp.App;
import com.grishberg.newsapp.mvp.common.RxUtils;
import com.grishberg.newsapp.mvp.data.api.ApiService;
import com.grishberg.newsapp.mvp.data.db.NewsItemsDao;
import com.grishberg.newsapp.mvp.data.models.RestResponse;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;
import com.grishberg.newsapp.mvp.views.NewsListView;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grishberg on 30.06.16.
 * Презентер отвечает за загрузку списка статей с сервера
 */
@InjectViewState
public class NewsListPresenter extends MvpPresenter<NewsListView> {
    private static final String TAG = NewsListPresenter.class.getSimpleName();
    @Inject
    ApiService apiService;
    @Inject
    NewsItemsDao newsItemsDao;

    public NewsListPresenter() {
        super();
        Log.d(TAG, "NewsListPresenter: ");
        App.getAppComponent().inject(this);
    }

    public Observable<RestResponse<List<NewsItem>>> getNewsList() {
        Log.d(TAG, "getNewsList: ");

        getViewState().showProgress();

        Observable<RestResponse<List<NewsItem>>> newsObservable = RxUtils
                .wrapRetrofitCall(apiService.getNewsList(),
                        id -> {
                            // extract cached data
                            Log.d(TAG, "getNewsList: check cache");
                            return newsItemsDao.getNewsList();
                        });

        // извлечь данные из запроса и поместить в кэш
        RxUtils.wrapAsync(newsObservable)
                .flatMap(listRestResponse -> {
                    // если данные из кэша, просто вернуть данные
                    if (listRestResponse.isCached()) {
                        Log.d(TAG, "flatMap: cached data");
                        return Observable.just(listRestResponse.getData());
                    }
                    // если данные из сети - обновить кэш
                    Log.d(TAG, "flatMap: data from network, need to save to db");
                    newsItemsDao.add(listRestResponse.getData());
                    newsItemsDao.removeOldData();
                    return Observable.just(newsItemsDao.getNewsList());
                })
                .subscribe(newsResult -> {
                            Log.d(TAG, "getNewsList: received data, thread: " + Thread.currentThread());
                            getViewState().hideProgress();
                            getViewState().showNewsList(newsResult);
                        }, exception -> {
                            Log.e(TAG, "getNewsList: except", exception);
                            getViewState().hideProgress();
                            getViewState().showError(exception.getMessage());
                        }

                );
        return newsObservable;
    }

    public void onErrorCancel() {
        getViewState().hideError();
    }
}
