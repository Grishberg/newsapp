package com.grishberg.newsapp.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.grishberg.datafacade.data.ListResult;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;

import java.util.List;

/**
 * Created by grishberg on 30.06.16.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface NewsListView extends MvpView {
    void showProgress();

    void hideProgress();

    void showError(String message);

    void hideError();

    void showNewsList(List<NewsItem> newsItems);
}
