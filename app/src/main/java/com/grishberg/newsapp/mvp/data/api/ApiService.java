package com.grishberg.newsapp.mvp.data.api;

import com.grishberg.newsapp.mvp.data.models.json.NewsDetails;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by grishberg on 30.06.16.
 */
public interface ApiService {
    @GET("/api/v1/articles")
    Call<List<NewsItem>> getNewsList();

    @GET("/api/v1/articles/{id}")
    Call<NewsDetails> getNewsDetails(@Path("id") int id);
}
