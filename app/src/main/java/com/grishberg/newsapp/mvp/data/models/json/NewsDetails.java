package com.grishberg.newsapp.mvp.data.models.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Модель для десериализации и хранения детальной информации о статье
 */
@DatabaseTable(tableName = "news_details")
public class NewsDetails {
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String TEXT = "text";
    public static final String USER_ID = "user_id";
    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER, columnName = ID, id = true)
    @SerializedName(ID)
    @Expose
    private int id;

    @DatabaseField(dataType = DataType.INTEGER, columnName = USER_ID)
    @SerializedName(USER_ID)
    @Expose
    private int userId;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = TITLE)
    @SerializedName(TITLE)
    @Expose
    private String title;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = TEXT)
    @SerializedName(TEXT)
    @Expose
    private String text;

    @SerializedName("date_create")
    @Expose
    private String dateCreate;
    @SerializedName("date_update")
    @Expose
    private String dateUpdate;
    @SerializedName("status")
    @Expose
    private int status;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The dateCreate
     */
    public String getDateCreate() {
        return dateCreate;
    }

    /**
     * @param dateCreate The date_create
     */
    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    /**
     * @return The dateUpdate
     */
    public String getDateUpdate() {
        return dateUpdate;
    }

    /**
     * @param dateUpdate The date_update
     */
    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    /**
     * @return The status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(int status) {
        this.status = status;
    }
}
