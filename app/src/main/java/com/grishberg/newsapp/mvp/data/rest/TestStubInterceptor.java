package com.grishberg.newsapp.mvp.data.rest;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.grishberg.newsapp.ApiConst;
import com.grishberg.newsapp.App;
import com.grishberg.newsapp.injection.RestModule;
import com.grishberg.newsapp.mvp.data.models.json.NewsItem;

import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by grishberg on 13.06.16.
 * Тестовый Interceptor который будет выдавать определенные ответы
 */
public class TestStubInterceptor implements Interceptor {
    private static final String TAG = TestStubInterceptor.class.getSimpleName();

    // Успешные ответы
    public static final String TEST_TITLE = "test title";
    public static final String TEST_TEXT = "test text";
    private static final SimpleDateFormat SDF = new SimpleDateFormat(RestModule.DATE_PATTERN, Locale.US);
    public static final Date DATE1 = makeDate(2016, 7, 1, 10, 36, 15);
    public static final Date DATE2 = makeDate(2016, 7, 1, 10, 37, 20);
    private final static String SUCCESS_LIST = "[  \n" +
            "  {\n" +
            "    \"id\": 1,\n" +
            "    \"title\": \"" + TEST_TITLE + "\",\n" +
            "    \"date_create\": \"" + SDF.format(DATE1) + "\",\n" +
            "    \"date_update\": \"" + SDF.format(DATE2) + "\"\n" +
            "  }\n" +
            "]";
    private static final String SUCCESS_DETAIL = "{\n" +
            "  \"id\": 1,\n" +
            "  \"user_id\": 1,\n" +
            "  \"title\": \"" + TEST_TITLE + "\",\n" +
            "  \"text\": \"" + TEST_TEXT + "\",\n" +
            "    \"date_create\": \"" + SDF.format(DATE1) + "\",\n" +
            "    \"date_update\": \"" + SDF.format(DATE2) + "\"\n" +
            "  \"status\": 1\n" +
            "}";

    // Ошибочные ответы
    private final boolean isSuccessResponses;

    public TestStubInterceptor(boolean isSuccessResponses) {
        this.isSuccessResponses = isSuccessResponses;
        App.getAppComponent().inject(this);
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response response = null;

        String responseString;
        // Извлечь URI запроса.
        final HttpUrl uri = chain.request().url();
        Log.d(TAG, "intercept: " + uri);
        // извлечь запускаемый метод.
        final String path = uri.encodedPath();
        Log.d(TAG, "intercept: path = " + path);

        int code = 200;

        if (isSuccessResponses) {
            // Примеры валидных ответов
            switch (path) {
                case ApiConst.Methods.GET_ARTICLE_LIST:
                    responseString = SUCCESS_LIST;
                    break;
                case ApiConst.Methods.GET_ARTICLE:
                    responseString = SUCCESS_DETAIL;
                    break;
                default:
                    responseString = "";
            }
        } else {
            // Примеры ошибочных ответов
            switch (path) {
                case ApiConst.Methods.GET_ARTICLE_LIST:
                    code = 403;
                    responseString = SUCCESS_LIST;
                    break;
                case ApiConst.Methods.GET_ARTICLE:
                    code = 403;
                    responseString = SUCCESS_DETAIL;
                    break;
                default:
                    code = 403;
                    responseString = "";
            }
        }
        response = new Response.Builder()
                .code(code)
                .message(responseString)
                .request(chain.request())
                .protocol(Protocol.HTTP_1_0)
                .body(ResponseBody.create(MediaType.parse("application/json"), responseString.getBytes()))
                .addHeader("content-type", "application/json")
                .build();

        return response;
    }

    private static Date makeDate(int year, int month, int date, int hh, int mm, int ss) {
        Calendar time = GregorianCalendar.getInstance();
        time.set(year, month, date, hh, mm, ss);
        time.set(Calendar.MILLISECOND, 0);
        return time.getTime();
    }
}